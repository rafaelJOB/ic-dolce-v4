import java.io.FileNotFoundException;
import java.io.IOException;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

public class Main {
	
	public static void main(String[] args) throws OWLOntologyStorageException, FileNotFoundException, IOException {

		if(args.length == 3) {
			
			DomainOntology domain = new DomainOntology();
			UpperOntology top = new UpperOntology();
			
			String domainPath = args[0];
			String topPath = args[1];
			String outFilePath = args[2];
			
			Matching matcher = new Matching(outFilePath);
			
			try {
				domain.ShouldLoad(domainPath);
				top.ShouldLoad(topPath);
			} catch (OWLOntologyCreationException e) {
				System.out.println("Ontology Creation Exception");
				e.printStackTrace();
			}
			
			domain.extract_concept();
			top.extract_concept();
			matcher.compare_2(domain, top);

		} else if(args.length == 4) {
			
			DomainOntology domain = new DomainOntology();
			UpperOntology top = new UpperOntology();
			
			String domainPath = args[0];
			String topPath = args[1];
			String outFilePath = args[2];
			int outSynsetTxt = Integer.parseInt(args[3]);
			
			if(domain.set_outFiles(outSynsetTxt, outFilePath)) {

				Matching matcher = new Matching(outFilePath);
			
				try {
					domain.ShouldLoad(domainPath);
					top.ShouldLoad(topPath);
				} catch (OWLOntologyCreationException e) {
					System.out.println("Ontology Creation Exception");
					e.printStackTrace();
				}
			
				domain.extract_concept();
				top.extract_concept();
				matcher.compare_2(domain, top);
				domain.get_outFiles(outSynsetTxt);
			}
			
		} else if(args.length == 5) {
			
			DomainOntology domain = new DomainOntology();
			UpperOntology top = new UpperOntology();
			
			String domainPath = args[0];
			String topPath = args[1];
			String outFilePath = args[2];
			int outSynsetTxt = Integer.parseInt(args[3]);
			String refFile = args[4];
			
			if(domain.set_outFiles(outSynsetTxt, outFilePath)) {

				Matching matcher = new Matching(outFilePath);
			
				try {
					domain.ShouldLoad(domainPath);
					top.ShouldLoad(topPath);
				} catch (OWLOntologyCreationException e) {
					System.out.println("Ontology Creation Exception");
					e.printStackTrace();
				}
			
				domain.extract_concept();
				top.extract_concept();
				matcher.compare_2(domain, top);
				
				Evaluator eva = new Evaluator(outFilePath ,refFile);
				eva.evaluate();
				domain.set_evaluator(eva.get_evaluator());
				domain.get_outFiles(outSynsetTxt);
			}
		
		} else {
			System.out.println("N�mero de argumentos n�o � v�lido!!");
		}
		
	}
				
}
