import java.io.IOException;

import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLClass;

public class UpperOntology extends Ontology {
	
	protected void extract_concept() throws IOException {
		
        for (OWLClass cls : ontology.getClassesInSignature()) {				//lista todas classes da ontologia

            Concept concept = new Concept();
            concept.set_ontologyID(ontologyID.toString());
            concept.set_ontologyName(ontologyID.getOntologyIRI().toString());
            
            concept.set_owlClass(cls);
            concept.set_classID(cls.toString());
            concept.set_className(cls.getIRI().getFragment().toString());
            
            concept.set_context(null);
            concept.set_distance(null);
            extract_annotation(cls, concept);
            //System.out.println(concept.get_className());
            listConcept.add(concept);
            //concept.print_info();
        }
	}
	
	private void extract_annotation(OWLClass cls, Concept cnp) {
		
		for(OWLAnnotation anno: cls.getAnnotations(ontology)) {
        	
        	String aux = anno.getValue().toString();
        	cnp.set_desc(aux);		//seta a descri��o do conceito
        }
	}

}
