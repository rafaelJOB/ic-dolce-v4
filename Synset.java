import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;

public class Synset {
	
	protected Concept concept;
	private IDictionary dict;
	
	private List<String> stopWords;
	private List<String> adjWords;
	
	private StanfordLemmatizer slem;
	
	protected void set_concept(Concept _concept) {
		concept = _concept;
	}
	
	protected void set_dictionary(IDictionary _dict) {
		dict = _dict;
	}
	
	protected void set_stpWords(List<String> _stopWords) {
		stopWords = _stopWords;
	}
	
	protected List<String> get_stpWords() {
		return stopWords;
	}
	
	protected void set_adjWords(List<String> _adjWords) {
		adjWords = _adjWords;
	}
	
	protected List<String> get_adjWords() {
		return adjWords;
	}
	
	protected void set_lemmatizer(StanfordLemmatizer _slem) {
		slem = _slem;
	}
	protected StanfordLemmatizer get_lemmatizer() {
		return slem;
	}
	
	private String toLemmatize(List<String> list) {
		String full = "";
		for(String word: list) {
			full = full + word + " ";
		}
		return full;
	}
	
	private HashSet<String> toSet(List<String> list) {
		HashSet<String> set = new HashSet<String>();
		for(String word: list) {
			set.add(word);
		}
		return set;
	}
	
	private List<String> toList(Set<String> set) {
		List<String> list = new ArrayList<String>();
		for(String word: set) {
			list.add(word);
		}
		return list;
	}
	
	void rc_goodSynset() throws IOException {		
		
		//System.out.println("CNP: " + concept.get_className());
		List<String> context = concept.transfer_context();
		
		String toLemma = toLemmatize(context);
		context.clear();
		context = slem.lemmatize(toLemma);

		concept.set_context(toSet(context));
		
		String name = concept.sp_conceptName();
		//System.out.println("SPNAME: " + name);
		
		List<String> cnpNameLemma = slem.lemmatize(name);
		int i = cnpNameLemma.size();
		name = cnpNameLemma.get(i - 1);
		//System.out.println("Name Lemma: " + name + "\n");
		
		dict.open();
		IIndexWord idxWord = dict.getIndexWord(name,POS.NOUN);

		LinkedHashMap<ISynset, List<String>> temp1 = new LinkedHashMap<ISynset, List<String>>();
		
		if(idxWord != null) {
			int numSy = 0;
			int max = 0;
			List<IWordID> wordIds = idxWord.getWordIDs();
			
			for (IWordID wordId : wordIds) {
				IWord word = dict.getWord (wordId) ;
			    ISynset synset = word.getSynset();
			    		
			    /*Cria uma lista com as sids do synset */
			    List<IWord> wordsSynset = synset.getWords();
			    String glossSynset = synset.getGloss();
			    		
			    /*Retorna uma lista com o BagOfWords do synset*/
			    List<String> bagSynset = createBagWords(wordsSynset, glossSynset);
			    
			    temp1.put(synset, bagSynset);

			    /*Metodo que compara o Contexto com o BagOfWords do synset*/
			    int size = intersection(context, bagSynset);
			    if(size > max) {
			    	max = size;
			    	concept.set_goodSynset(synset);
						//System.out.println(max);
			    }
			    numSy++;
			}
			concept.set_numSy(numSy);
		}
		concept.set_synsetCntx(temp1);
		dict.close();		
	}
	
	private List<String> createBagWords(List<IWord> wordsSynset, String glossSynset) {
	    List<String> list = new ArrayList<String>();
	    
	    for (IWord i : wordsSynset) {
	    	StringTokenizer st = new StringTokenizer(i.getLemma().toLowerCase().replace("_"," ")," ");
	    	while (st.hasMoreTokens()) {
	    		  String token = st.nextToken();
	    	 	  if (!list.contains(token)) {
	    	  	      list.add(token);
	    	      }
	    	}
	    }
	    glossSynset = glossSynset.replaceAll(";"," ").replaceAll("\"", " ").replaceAll("-"," ").toLowerCase();
	    //System.out.println(stopWords);
	    StringTokenizer st = new StringTokenizer(glossSynset," ");
    	while (st.hasMoreTokens()) {
    		   String token = st.nextToken().toLowerCase();
    		   token = rm_specialChar(token);
    		   if (!stopWords.contains(token) && !list.contains(token)) {			
    			   list.add(token);
    		   }
    	}
    	
    	String toLemma = toLemmatize(list);
		list.clear();
		list = slem.lemmatize(toLemma); 
		Set<String> set = new HashSet<String>();
		set = toSet(list);
		list.clear();
		list = toList(set);   	 	
    	    		    
	   return list;
	}
	
	/*Fun��o que retorna o n�mero de elementos iguais entre duas listas*/
	int intersection(List<String> context, List<String> bagSynset) {
		int inter = 0;
		for(String word: context) {
			//System.out.println(word);
			word = word.toLowerCase();
			for(String wordCompared: bagSynset) {
				if(word.equals(wordCompared)) {
					inter++;
				}
			}
		}
		return inter;
	}

	List<String> rm_adj(List<String> wordList) {
		
		List<String> temp = new ArrayList<String>();  
		    for(String word: wordList) {
		    	temp.add(word);
				if(adjWords.contains(word.toLowerCase())) {
		    		temp.remove(word);
		    	}
		    }    
		    wordList.clear();
		    wordList = temp;
		    return wordList;
	}
	
	/*remove as stopwords de uma list*/
	List<String> rm_stopWords(List<String> wordCList) {
		    
		List<String> temp = new ArrayList<String>();
			for(String wordC: wordCList) {

		        if(!stopWords.contains(wordC.toLowerCase())) {
		        	temp.add(wordC.toLowerCase());
		        	//System.out.println("SEM STOP: " + wordC);     
		        }
		        		          
		    }
			wordCList.clear();
			wordCList = temp;
			return wordCList;
	}
	
	/*remove caracteres especiais*/
	String rm_specialChar(String word) {
		
		if(word.contains("(")) {
        	word = word.replace("(", "");
        }
        
        if(word.contains(")")) {
        	word = word.replace(")", "");
        }
        
        if(word.contains(",")) {
        	word = word.replace(",", "");
        }
        
        if(word.contains(":")) {
        	word = word.replace(":", "");
        }        
        
        if(word.contains("'")) {
        	word = word.replace("'", "");
        }
        
        if(word.contains(".")) {
        	word = word.replace(".", "");
        }
        
        if(word.contains("?")) {
        	word = word.replace("?","");
        }
        
        if(word.contains("!")) {
        	word = word.replace("!","");
        }
        
        return word;	
	}

}

