import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import edu.mit.jwi.item.ISynset;

public class Matching {

	private List<Mapping> listMap;
	private String localfile;
	
	public Matching(String _file) {
		listMap = null;
		localfile = _file;
	}

	
	private String toRDF(Mapping m) {
		
		String out = "\t<map>\n" +
				"\t\t<Cell>\n" +
				"\t\t\t<entity1 rdf:resource='"+ m.get_target() +"'/>\n" +
				"\t\t\t<entity2 rdf:resource='"+ m.get_source() +"'/>\n" +
				"\t\t\t<relation>" + m.get_relation().toString() + "</relation>\n" +
				"\t\t\t<measure rdf:datatype='http://www.w3.org/2001/XMLSchema#float'>"+ m.get_measure() +"</measure>\n" +
				"\t\t</Cell>\n" + "\t</map>\n";
		return out;		
	}
	
	void out_rdf(Ontology onto1, Ontology onto2) {
		
		try {
			FileWriter arq = new FileWriter(localfile);
			PrintWriter print = new PrintWriter(arq);
		
			print.print("<?xml version='1.0' encoding='utf-8' standalone='no'?>\n" + 
						"<rdf:RDF xmlns='http://knowledgeweb.semanticweb.org/heterogeneity/alignment#'\n" +
						"\t\t xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'\n" +
						"\t\t xmlns:xsd='http://www.w3.org/2001/XMLSchema#'\n" + 
						"\t\t xmlns:align='http://knowledgeweb.semanticweb.org/heterogeneity/alignment#'>\n");
		
			print.print("<Alignment>\n" + 
						"\t<xml>yes</xml>\n" + 
						"\t<level>0</level>\n" + 
						"\t<type>11</type>\n");
		
			print.print("\t<onto1>\n" + "\t\t<Ontology rdf:about=" + '"' + onto2.get_ontologyID().getOntologyIRI().toString() + '"' + ">\n" + 
						"\t\t\t<location>file:" + onto1.get_fileName() + "</location>\n" + 
							"\t\t</Ontology>\n" + "\t</onto1>\n");
		
			print.print("\t<onto2>\n" + "\t\t<Ontology rdf:about=" + '"' + onto1.get_ontologyID().getOntologyIRI().toString() + '"' + ">\n" + 
				"\t\t\t<location>file:" + onto2.get_fileName() + "</location>\n" + 
					"\t\t</Ontology>\n" + "\t</onto2>\n");
		
			for(Mapping m: listMap) {
				if(!m.get_measure().equals("false")) {
					print.print(toRDF(m));				//adc if
				}
			}
		
			print.print("</Alignment>\n" + "</rdf:RDF>");
		
			arq.close();
			System.out.println("Arquivo .RDF gerado!");
			
		} catch(IOException e) {
			System.out.println("Opera��o I/O interrompida, no arquivo de sa�da .RDF!");
	    	System.out.println("erro: " + e);
			
		}
	}
	
	OWLClassExpression extract_superClass(OWLClass cls, Ontology onto2) {
		
		OWLClassExpression last = null;
		for(OWLClassExpression sup: cls.getSuperClasses(onto2.get_ontology())) {
        	
        	if(!sup.isAnonymous()) {					
        		//System.out.println("SuperClasseID: " + sup);
        		//System.out.println("SuperClasse: " + sup.asOWLClass().getIRI().getFragment() + "\n");

        		last = extract_superRecurClass(sup, onto2);
        		if(last == null) {
        			last = sup;
        		}
        		//System.out.println(last.toString());	
        	}	
		}

		return last;
	}
	
	
	OWLClassExpression extract_superRecurClass(OWLClassExpression su, Ontology onto2) {	
		OWLClassExpression last = null;
		if(su != null) {

			for(OWLClassExpression sup: ((OWLClass) su).getSuperClasses(onto2.get_ontology())) {
        	
				if(!sup.isAnonymous()) {					
					//System.out.println("SuperClasseID: " + sup);
					//System.out.println("SuperClasse: " + sup.asOWLClass().getIRI().getFragment() + "\n");

					last = extract_superRecurClass(sup, onto2);
					if(last == null) {
						last = sup;
					}
					//System.out.println(last.toString());
				}	
			}
		}

		return last;
	}
		
	public void compare_2(Ontology onto1, Ontology onto2) {
		List<Concept> list_1 = onto1.get_listConcepts();
		List<Concept> list_2 = onto2.get_listConcepts();
		OWLClassExpression last = null;
		List<Mapping> listM = new ArrayList<Mapping>();

		for(Concept cnp_1: list_1) {
		
			Mapping map = new Mapping();
			ISynset synset = cnp_1.get_goodSynset();
			cnp_1.print_info();

			if(synset != null) {
				OWLClass cls = new_search(cnp_1, list_2);
			
				if(cls != null) {
					System.out.println("Ali Concept: " + cls.getIRI().getFragment().toString());
					last = extract_superClass(cls, onto2);
				
					if(last != null) {
						System.out.println("Ali Concept Top: " + last.asOWLClass().getIRI().getFragment().toString());
						cnp_1.set_aliClass(last.asOWLClass());
				
					} else {
						cnp_1.set_aliClass(cls);
					}
			
					map.set_source(cnp_1.get_owlClass().getIRI().toString());
					map.set_target(cnp_1.get_aliClass().getIRI().toString());		//alterado de ali.toString || alterado de get_classID
					map.set_relation("&lt;");
					//map.set_relation("=");
					//map.set_relation("<");
					//map.set_measure("true");
					map.set_measure("1.0");
			
					System.out.println("-----------");
					listM.add(map);
				} else {
					System.out.println("Ali Concept: " + "null");
					cnp_1.set_aliClass(null);
					map.set_source(cnp_1.get_classID());
					map.set_target("null");
					map.set_relation("null");
					map.set_measure("false");
				
					System.out.println("-----------");
					listM.add(map);
				}
			} else {

				map.set_source(cnp_1.get_classID());
				map.set_target("null");
				map.set_relation("null");
				map.set_measure("false");
			
				System.out.println("-----------");
				listM.add(map);
			}
		}
		listMap = listM;
		out_rdf(onto1, onto2);
	}
	
	
	
	private OWLClass new_search(Concept cnp_1, List<Concept> topConceptList) {			
		OWLClass clss = null;
		Set<String> glossList = new HashSet<String>();
		String gloss = cnp_1.get_goodSynset().getGloss();

		int max = 0;
		glossList = sp_string(gloss);
		glossList = rm_specialChar(glossList);
		//System.out.println(glossList);
		for(Concept cnp_2: topConceptList) {
			if(cnp_2.get_desc() != null) {
				
				Set<String> descList = new HashSet<String>();
				int size = 0;
				
				String desc = cnp_2.get_desc();
				descList = sp_string(desc); 
			
				descList = rm_specialChar(descList);
				size = intersection(glossList, descList);
				if(size > max) {
					max = size;
					clss = cnp_2.get_owlClass();
					//System.out.println(descList + " | " + size);
				}
			}
			
		}
		return clss;
	}
	
	private Set<String> rm_specialChar(Set<String> list) {
		
		Set<String> temp = new LinkedHashSet<String>();
		char x = '"';
		String z = String.valueOf(x);
		for(String word: list) {
			
			if(word.contains(z)) {
				word = word.replace(z, "");
			}
			
			if(word.endsWith("-")) {
				word = word.replace("-", "");
			}
		
			if(word.contains("(")) {
				word = word.replace("(", "");
			}
        
			if(word.contains(")")) {
				word = word.replace(")", "");
			}
        
			if(word.contains(",")) {
				word = word.replace(",", "");
			}
        
			if(word.contains(":")) {
				word = word.replace(":", "");
			}        
        
			if(word.contains("'s")) {
				word = word.replace("'s", "");		//adc remo��o " 's "
			}
        
			if(word.contains("'")) {
				word = word.replace("'", "");
			}
        
			if(word.contains("?")) {
				word = word.replace("?", "");
			}
        
			if(word.contains("!")) {
				word = word.replace("!", "");
			}
        
			if(word.contains(".")) {
				word = word.replace(".", "");
			}
        
			if(word.contains(";")) {
				word = word.replace(";", "");
			}
			
			if(word.contains("\\")) {
				word = word.replace("\\", "");
			}
			temp.add(word);
		}
		list.clear();
		list = temp;
        return list;	
	}
	
	private Set<String> sp_string(String str) {
		Set<String> temp= new HashSet<String>();

			String[] split = str.split(" |�");	
			
			for(String strSplit: split) {
				temp.add(strSplit.toLowerCase());
			}
			return temp;
		}
	
	private int intersection(Set<String> list_1, Set<String> list_2) {
		int inter = 0;
		
		for(String word_1: list_1) {
			
			word_1 = word_1.toLowerCase();
			for(String word_2: list_2) {
				
				if(word_1.equals(word_2)) {
					inter++;
					break;
				}
			}	
		}
		return inter;
	}
	
}

