
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;

import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.IWord;
import fr.inrialpes.exmo.align.impl.eval.PRecEvaluator;

public class DomainOntology extends Ontology {
	String fileSy;
	String fileSy2;
	String fileSy3;
	PRecEvaluator evaluator;
	
	public void set_fileSy(String file) {
		fileSy = file;
	}
	
	public void set_fileSy_2(String file) {
		fileSy2 = file;
	}
	
	public void set_fileSy_3(String file) {
		fileSy3 = file;
	}
	
	protected void set_evaluator(PRecEvaluator _eva) {
		this.evaluator = _eva;
	}
	
	public void extract_concept() throws IOException {
		
		BaseSynset base = new BaseSynset();
		base.init();
		
		StanfordLemmatizer slem = new StanfordLemmatizer();
		
        for (OWLClass cls : ontology.getClassesInSignature()) {				//lista todas classes da ontologia
        	
           //System.out.println("ClasseID: " + cls); 						//+ "SuperClasses: " + cls.getSuperClasses(local)
           //System.out.println("Classe: " + cls.getIRI().getFragment() + "\n");
            Synset synset = new Synset();
            synset.set_dictionary(base.get_dictionary());
            synset.set_stpWords(base.get_stpWords());
            synset.set_adjWords(base.get_adjWords());
            
            synset.set_lemmatizer(slem);
        	
            Concept concept = new Concept();
            concept.set_ontologyID(ontologyID.toString());
            concept.set_ontologyName(ontologyID.getOntologyIRI().toString());
            
            concept.set_owlClass(cls);
            concept.set_classID(cls.toString());
            concept.set_className(cls.getIRI().getFragment().toString());
            
            //System.out.println(concept.get_className() + " ORDEM");
            HashSet<String> set = new HashSet<String>();
            TreeMap<String, Integer> map = new TreeMap<String, Integer>();
            List<OWLClassExpression> listSup = new ArrayList<OWLClassExpression>();
            List<OWLClassExpression> listSub = new ArrayList<OWLClassExpression>();
            
            map.put(cls.getIRI().getFragment().toString(), 0);
            set.add(cls.getIRI().getFragment().toString());
            
            /* pega a anota��o da classe atual */
            extract_annotation(cls, concept, set);
            
            /* pega super classe e sua anota��o da classe regente */
            extract_superClass(cls, set, map, listSup);
            
            /* pega a sub classe e a anota��o da  classe regente */
            extract_subClass(cls, set, map, listSub);
            
            concept.set_context(set);
            concept.set_stpWords(base.get_stpWords());
            concept.set_distance(map);
            concept.set_supers(listSup);
            concept.set_subs(listSub);
            
            concept.init(); 			//processa o contexto!
            
            listConcept.add(concept);
                     
            synset.set_concept(concept);
            
            /*chama o metodo para recuperar o synset adequado*/           
            synset.rc_goodSynset();	
        }
	}
	
	private void extract_annotation(OWLClass cls, Concept cnp, HashSet<String> set) {
		
		for(OWLAnnotation anno: cls.getAnnotations(ontology)) {
			if(anno.getProperty().getIRI().getFragment().equals("comment")) { //pode ser "definition"
				String aux = anno.getValue().toString();
				aux = rm_suffix(aux);
				cnp.set_desc(aux);		//seta a descri��o do conceito
        	
				aux = remove_specialChar(aux);
				set.add(aux);
			}
        }
	}
	
	private String remove_specialChar(String word) {
		String aux = word;
		char x = '"';
		String z = String.valueOf(x);
    	
    	if(aux.contains("  ")) {
    		aux = aux.replaceAll("  ", " ");
    	}

    	if(aux.contains(z)) {
    		aux = aux.replace(z, "");
    	}
    	
    	if(aux.contains(".")) {
    		aux = aux.replace(".", " ");
    	}
    	
    	if(aux.contains(",")) {
    		aux = aux.replace(",", "");
    	}
    	
    	if(aux.contains("?")) {		
    		aux = aux.replace("?", " ");
    	}
    	
    	if(aux.contains(":")) {
    		aux = aux.replace(":", " ");
		}
    	
    	if(aux.contains("!")) {
    		aux = aux.replace("!", " ");
		}
    	
    	if(aux.contains("  ")) {
    		aux = aux.replaceAll("  ", " ");
    	}
    	return aux;
    	
	}
	
	private String rm_suffix(String aux) {
		if(aux.endsWith("^^xsd:string")) {
			aux = aux.replace("^^xsd:string", "");
		}
		if(aux.endsWith("@en")) {
			aux = aux.replace("@en", "");
		}
		
		return aux;
	}
	
	protected void get_outFiles(int index) {
		if(index == 0) {
			
		} else if(index == 1) {
			out_syCNTXT();
		} else if(index == 2) {
			out_syCNTXT_2();
		} else if(index == 3) {
			out_syCNTXT_3();
		} else if(index == 4) {
			out_syCNTXT();
			out_syCNTXT_2();
			out_syCNTXT_3();
		} 
	}
	
	protected boolean set_outFiles(int index, String path) {
		if(index == 0) {
			return true;
		} else if(index == 1) {
			path = path.replace(".rdf", "SUFFIX");
			String path_1 = path.replace("SUFFIX", "_1.txt");
			set_fileSy(path_1);
			return true;
			
		} else if(index == 2) {
			path = path.replace(".rdf", "SUFFIX");
			String path_2 = path.replace("SUFFIX", "_2.txt");
			set_fileSy_2(path_2);
			return true;
			
		} else if(index == 3) {
			path = path.replace(".rdf", "SUFFIX");
			String path_3 = path.replace("SUFFIX", "_3.txt");
			set_fileSy_3(path_3);
			return true;
			
		} else if(index == 4) {	
			path = path.replace(".rdf", "SUFFIX");
			String path_1 = path.replace("SUFFIX", "_1.txt");
			String path_2 = path.replace("SUFFIX", "_2.txt");		
			String path_3 = path.replace("SUFFIX", "_3.txt");
			
			set_fileSy(path_1);
			set_fileSy_2(path_2);
			set_fileSy_3(path_3);
			return true;
			
		} else {
			System.out.println("Inteiro passado por argumento fora do limite!! Tente 0, 1, 2 , 3 ou 4!!");
			return false;
		}
	}
	
	void out_syCNTXT() {
		float media = 0;
		int aux = 0;
		int max = 0;
		Concept maior = new Concept();
		try {
			FileWriter arq = new FileWriter(fileSy); 
			PrintWriter print = new PrintWriter(arq);
		
			for(Concept cnp: listConcept) {
				List<String> bgwSelect  = new ArrayList<String>();
				print.printf("Conceito: " + cnp.get_className() + "%n");
				if(cnp.get_desc() != null) {
					print.printf("Desc:" + cnp.get_desc() + "%n");
				}
				print.printf("Super: ");
				for(OWLClassExpression sup: cnp.get_supers()) {
					print.printf(sup.asOWLClass().getIRI().getFragment().toString() + " ");
				}
			
				print.printf("%n" + "Sub: ");
				for(OWLClassExpression sub: cnp.get_subs()) {
					print.printf(sub.asOWLClass().getIRI().getFragment().toString() + " ");
				}
			
				print.printf("%n%n");
				print.printf("Contexto:");
			
				print.printf(cnp.get_context().toString());

				print.printf("%n%n");
			
				Map<ISynset, List<String>> temp = new LinkedHashMap<ISynset, List<String>>();
				temp = cnp.get_synsetCntx();
			
				if(temp != null) {
					print.printf("Conjunto de synsets: " + "%n");
					for (Entry<ISynset, List<String>> entry : temp.entrySet()) {					
						String key = entry.getKey().toString();
						List<String> value = entry.getValue();
						
						if(cnp.get_goodSynset() != null && cnp.get_goodSynset().equals(entry.getKey())) {
							bgwSelect = entry.getValue();
						}
						print.printf("Synset: " + key + " | " + entry.getKey().getGloss() + "%n");
						print.printf("BOW:");

						print.printf(value.toString());

						print.printf("%n%n");
					}
				}
				print.printf("%n" + "SELECIONADO: ");
				if(cnp.get_goodSynset() != null) {
					print.printf(cnp.get_goodSynset().toString() + " | " + cnp.get_goodSynset().getGloss().toString() + "%n");
					
					aux++;
					media = media + cnp.get_numSy();
					
					print.printf("Intersec��o de palavras:");
					for(String a: bgwSelect) {
						if(cnp.get_context().contains(a)) {
							print.printf(" " + a);
						}					
					}
					print.printf("%n");
					
				} else {
					print.printf("Synset: NULO" + "%n");
				}
			
				if(cnp.get_aliClass() != null) {
					print.printf("Conceito DOLCE: " + cnp.get_aliClass().getIRI().getFragment().toString() + "%n");
					print.printf("-----------" + "%n");
				} else {
					print.printf("Conceito DOLCE: NULO" + "%n");
					print.printf("-----------" + "%n");
				}
				
				if(!(cnp.get_className().toLowerCase().equals("thing")) && cnp.get_numSy() > max) {
					max = cnp.get_numSy();
					maior = cnp;
				}
			}
			media = media / aux;
			
			print.printf("A media de synsets recuperados: " + media + "%n");
			print.printf("O conceito com maior numero de synsets recuperados: " + maior.get_className() + "| " + maior.get_numSy() + "%n");

			for(Concept cnp: listConcept) {

				print.printf(cnp.get_className() + "| " + cnp.get_numSy() + "%n");
				if(cnp.get_numSy() != -1) {
				}
			}
			
			PRecEvaluator eva = this.evaluator;
			if(eva != null) {
				print.printf("Medidas: REF - ALI%n");
				print.printf("F-Measure: " + eva.getFmeasure() + "%nPrecision:  " + eva.getPrecision() + "%nRecall: " + eva.getRecall() + "%nOverall: " + eva.getOverall() + "%n");
			}
			arq.close();
			System.out.println("Arquivo Synset X Contexto gerado!");
	
		} catch(IOException e) {
			System.out.println("Opera��o I/O interrompida, no arquivo de sa�da syCNTXT!");
	    	System.out.println("erro: " + e);
		}
	}
	
	void out_syCNTXT_2() {
		float media = 0;
		int aux = 0;
		int max = 0;
		Concept maior = new Concept();
		try {
			FileWriter arq = new FileWriter(fileSy2); 
			PrintWriter print = new PrintWriter(arq);
		
			for(Concept cnp: listConcept) {
				List<String> bgwSelect  = new ArrayList<String>();
				print.printf("Conceito: " + cnp.get_className() + "%n");
				if(cnp.get_desc() != null) {
					print.printf("Desc:" + cnp.get_desc() + "%n");
				}
				print.printf("Super:");
				for(OWLClassExpression sup: cnp.get_supers()) {
					print.printf(" " + sup.asOWLClass().getIRI().getFragment().toString());
				}
			
				print.printf("%n" + "Sub:");
				for(OWLClassExpression sub: cnp.get_subs()) {
					print.printf(" " + sub.asOWLClass().getIRI().getFragment().toString());
				}
			
				print.printf("%n%n");
				print.printf("Contexto:");
			
				for(String a: cnp.get_context()) {
					print.printf(" " + a);
				}
				print.printf("%n%n");
			
				Map<ISynset, List<String>> temp = new LinkedHashMap<ISynset, List<String>>();
				temp = cnp.get_synsetCntx();
			
				if(temp != null) {
					print.printf("Conjunto de synsets: " + "%n");
					for (Entry<ISynset, List<String>> entry : temp.entrySet()) {					
						List<IWord> key = entry.getKey().getWords();
						
						if(cnp.get_goodSynset() != null && cnp.get_goodSynset().equals(entry.getKey())) {
							bgwSelect = entry.getValue();
						}
						
						print.printf("Synset Words:");
						for(IWord a: key) {
							print.printf(" " + a.getLemma());
						}
						List<String> value = entry.getValue();
			    
						print.printf(" | " + entry.getKey().getGloss() + "%n");
						print.printf("BOW:");
						for(String word: value) {
							print.printf(" " + word);
						}
			    
						print.printf("%n%n");
					}
				}
				print.printf("%n" + "SELECIONADO:");
				if(cnp.get_goodSynset() != null) {
					List<IWord> key = cnp.get_goodSynset().getWords();
					
					for(IWord a: key) {
						print.printf(" " + a.getLemma());
					}
					print.printf(" | " + cnp.get_goodSynset().getGloss() + "%n");
					
					aux++;
					media = media + cnp.get_numSy();
					print.printf("Intersec��o de palavras:");
					for(String a: bgwSelect) {
						if(cnp.get_context().contains(a)) {
							print.printf(" " + a);
						}					
					}
					print.printf("%n");
				} else {
					print.printf("Synset: NULO" + "%n");
				}
			
				if(cnp.get_aliClass() != null) {
					print.printf("Conceito DOLCE: " + cnp.get_aliClass().getIRI().getFragment().toString() + "%n");
					print.printf("-----------" + "%n");
				} else {
					print.printf("Conceito DOLCE: NULO" + "%n");
					print.printf("-----------" + "%n");
				}
				
				if(!(cnp.get_className().toLowerCase().equals("thing")) && cnp.get_numSy() > max) {
					max = cnp.get_numSy();
					maior = cnp;
				}
			}
			media = media / aux;
			
			print.printf("A media de synsets recuperados: " + media + "%n");
			print.printf("O conceito com maior numero de synsets recuperados: " + maior.get_className() + "| " + maior.get_numSy() + "%n");

			for(Concept cnp: listConcept) {

				print.printf(cnp.get_className() + "| " + cnp.get_numSy() + "%n");
				if(cnp.get_numSy() != -1) {
				}
			}

			PRecEvaluator eva = this.evaluator;
			print.printf("%nMedidas: REF - ALI%n");
			print.printf("F-Measure: " + eva.getFmeasure() + "%nPrecision:  " + eva.getPrecision() + "%nRecall: " + eva.getRecall() + "%nOverall: " + eva.getOverall() + "%n");
			
			arq.close();
			System.out.println("Arquivo Synset X Contexto 2 gerado!");
	
		} catch(IOException e) {
			System.out.println("Opera��o I/O interrompida, no arquivo de sa�da syCNTXT_2!");
	    	System.out.println("erro: " + e);
		}
	}
	
	void out_syCNTXT_3() {
		float media = 0;
		int aux = 0;
		int max = 0;
		Concept maior = new Concept();
		try {
			FileWriter arq = new FileWriter(fileSy3); 
			PrintWriter print = new PrintWriter(arq);
		
			for(Concept cnp: listConcept) {
				
				if( !(cnp.get_className().toLowerCase().equals("thing")) && (cnp.get_supers().isEmpty() || cnp.get_supers().get(0).asOWLClass().getIRI().getFragment().toLowerCase().equals("thing")) ) {
					
					List<String> bgwSelect  = new ArrayList<String>();
					print.printf("Conceito: " + cnp.get_className() + "%n");
					
					if(cnp.get_desc() != null) {
						print.printf("Desc:" + cnp.get_desc() + "%n");
					}
					
					print.printf("Super:");
					
					for(OWLClassExpression sup: cnp.get_supers()) {
						print.printf(" " + sup.asOWLClass().getIRI().getFragment().toString());
					}
			
					print.printf("%n" + "Sub:");
					
					for(OWLClassExpression sub: cnp.get_subs()) {
						print.printf(" " + sub.asOWLClass().getIRI().getFragment().toString());
					}
			
					print.printf("%n%n");
					print.printf("Contexto:");
					
					for(String a: cnp.get_context()) {
						print.printf("," + a);
					}
					print.printf("%n%n");
			
					Map<ISynset, List<String>> temp = new LinkedHashMap<ISynset, List<String>>();
					temp = cnp.get_synsetCntx();
			
					if(temp != null) {
						
						print.printf("Conjunto de synsets: " + "%n");
						
						for (Entry<ISynset, List<String>> entry : temp.entrySet()) {					
							List<IWord> key = entry.getKey().getWords();
						
							if(cnp.get_goodSynset() != null && cnp.get_goodSynset().equals(entry.getKey())) {
								bgwSelect = entry.getValue();
							}
						
							print.printf("Synset Words:");
							
							for(IWord a: key) {
								print.printf(" " + a.getLemma());
							}
							List<String> value = entry.getValue();
			    
							print.printf(" | " + entry.getKey().getGloss() + "%n");
							print.printf("BOW:");
							for(String word: value) {
								print.printf("," + word);
							}
			    
							print.printf("%n%n");
						}
					}
					
					print.printf("%n" + "SELECIONADO:");
					
					if(cnp.get_goodSynset() != null) {
						
						List<IWord> key = cnp.get_goodSynset().getWords();
					
						for(IWord a: key) {
							print.printf(" " + a.getLemma());
						}
						
						print.printf(" | " + cnp.get_goodSynset().getGloss() + "%n");
					
						print.printf("Intersec��o de palavras:");
						
						for(String a: bgwSelect) {
							
							if(cnp.get_context().contains(a)) {
								print.printf(" " + a);
							}					
						}
						print.printf("%n");
						
					} else {
						print.printf("Synset: NULO" + "%n");
					}
			
					if(cnp.get_aliClass() != null) {
						print.printf("Conceito DOLCE: " + cnp.get_aliClass().getIRI().getFragment().toString() + "%n");
						print.printf("-----------" + "%n");
					} else {
						print.printf("Conceito DOLCE: NULO" + "%n");
						print.printf("-----------" + "%n");
					}
				
					if(!(cnp.get_className().toLowerCase().equals("thing")) && cnp.get_numSy() > max) {
						max = cnp.get_numSy();
						maior = cnp;
					}	
				}
			}
			
			print.printf("O conceito com maior numero de synsets recuperados: " + maior.get_className() + "| " + maior.get_numSy() + "%n");

			for(Concept cnp: listConcept) {
				if( !(cnp.get_className().toLowerCase().equals("thing")) && (cnp.get_supers().isEmpty() || cnp.get_supers().get(0).asOWLClass().getIRI().getFragment().toLowerCase().equals("thing") )) {
					print.printf(cnp.get_className() + "| " + cnp.get_numSy() + "%n");
					if(cnp.get_numSy() != -1) {
						aux++;
						media = media + cnp.get_numSy();
					}
				}
			}
			media = media / aux;
			print.printf("A media de synsets recuperados: " + media + "%n");
			print.printf("Numero de conceitos da media: " + aux);
			
			PRecEvaluator eva = this.evaluator;
			print.printf("%nMedidas: REF - ALI%n");
			print.printf("F-Measure: " + eva.getFmeasure() + "%nPrecision:  " + eva.getPrecision() + "%nRecall: " + eva.getRecall() + "%nOverall: " + eva.getOverall() + "%n");
			
			arq.close();
			System.out.println("Arquivo Synset X Contexto 3 gerado!");
			
		} catch(IOException e) {
			System.out.println("Opera��o I/O interrompida, no arquivo de sa�da syCNTXT_2!");
	    	System.out.println("erro: " + e);
		}
	}
	
	public boolean hasUp(String word) {
		
		int x = word.length();
		
		for(int y = 1; y < x; y++) {
			if(Character.isUpperCase(word.charAt(y))) {
				return true;
			}	
		}
		return false;	
	}
	
	
}

